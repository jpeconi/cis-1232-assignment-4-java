package assignment4;

/**
 *
 * @author jpeconi
 * @since 02/22/2016 CIS 1232 - Assignment 4
 *
 * This is the main class for assignment 4. This is a login program using
 * HashMaps and ArrayLists. This class controls the program. It creates an
 * instance of the login class.
 */
public class Assignment4 {

    public static Login login = new Login();
    public static String[] userNameArray = {"John", "Steve", "Bonnie", "Kylie", "Logan", "Robert"};
    public static String[] passwordArray = {"1111", "2222", "3333", "4444", "5555", "6666"};
    public static final String MENU = "1) Load \n"
            + "2) Login\n"
            + "3) Show\n"
            + "4) Exit\n";

    /**
     *
     * @author jpeconi
     * @since 02/22/2016 CIS 1232 - Assignment 4
     *
     * The main method. This method controls the program. Displays the menu and
     * then calls the process menu method.
     */
    public static void main(String[] args) {
        int userChoice;
        do {
            System.out.print(MENU);
            userChoice = Utility.promptInt("Enter your choice --> ");
            processMenu(userChoice);
        } while (userChoice != 4);

    }

    /**
     *
     * @author jpeconi
     * @since 02/22/2016 CIS 1232 - Assignment 4
     *
     * This is the process menu method. This method controls the menu. It
     * accepts the user choice from the main method and performs a switch on it.
     */
    public static void processMenu(int userChoice) {
        switch (userChoice) {
            case 1:
                login.loadCredentials(userNameArray, passwordArray);
                System.out.println("");
                break;
            case 2:
                login.loginAttempt();
                break;
            case 3:
                login.showAttempts();
                break;
            case 4:
                System.out.println("GoodBye");
                break;
            default:
                System.out.println("Invalid Option!");
        }
    }

}
