package assignment4;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author jpeconi
 * @since 02/22/2016 CIS 1232 - Assignment 4
 *
 * Login class. This class contains the HashMap that will have the usernames and
 * passwords loaded into it. It also contains the array list with a record of
 * all the usernames and passwords that were attempted.
 */
public class Login {

    private HashMap logins = new HashMap();
    private ArrayList<String> loginAttempts = new ArrayList();

    /**
     *
     * @author jpeconi
     * @since 02/22/2016 CIS 1232 - Assignment 4
     *
     * This method loads the usernames from the userName array and the passwords
     * from the password array. These are passed in from the calling method. It
     * then assigns the username to the key and password to the value in the
     * hashmap.
     */
    public void loadCredentials(String[] userNameArray, String[] passArray) {
        for (int i = 0; i < userNameArray.length; i++) {
            logins.put(userNameArray[i], passArray[i]);
        }
    }

    /**
     *
     * @author jpeconi
     * @since 02/22/2016 CIS 1232 - Assignment 4
     *
     * The login attempt class. This class prompts the user for a username and
     * password. It then compares the values towards the key and value pairs in
     * the hashmap. Tells the user whether it was succesful or not, then adds
     * the entry to the array list file.
     */
    public void loginAttempt() {
        boolean isCorrect = false;

        if (logins.isEmpty()) {
            System.out.println("\nYou must load the credentials first!\n");
        } else {
            String username = Utility.promptString("Enter your username --> ");
            String password = Utility.promptString("Enter password for --> ");

            if (logins.containsKey(username)) {
                if (logins.get(username).equals(password)) {
                    System.out.println("\nLogin Success\n");
                    isCorrect = true;
                } else {
                    System.out.println("\nLogin Failed\n");
                }
            } else {
                System.out.println("\nLogin Failed\n");
            }

            loginAttempts.add(username + "/" + password + "/" + isCorrect);
        }
    }

    /**
     *
     * @author jpeconi
     * @since 02/22/2016 CIS 1232 - Assignment 4
     *
     * This method loops through the array list of attempts and displays it to
     * the user.
     */
    public void showAttempts() {
        System.out.println("");
        if (loginAttempts.isEmpty()) {
            System.out.println("No logins have been entered!\n");
        } else {
            for (String attempt : loginAttempts) {
                System.out.println(attempt);
            }
            System.out.println("");
        }
    }

}
