package assignment4;

import java.util.Scanner;

/**
 * @author Jamison Peconi
 * @since 01/18/2016
 *
 * This class is the Utility class. This class contains tools used in other
 * classes.
 */
public class Utility {

    //Universal variables to be used in other Java Programs
    public static Scanner input = new Scanner(System.in);
    public static final String TAB = "\t";

    /**
     * @jpeconi @date 02/01/2016
     * @return
     *
     * This is the getter to access the scanner.
     */
    public static Scanner getInput() {
        return input;
    }

    public static String promptString(String question) {
        System.out.print(question);
        return input.nextLine();
    }

    /**
     * @author jpeconi
     * @date 02/01/2016
     * @param question
     * @return
     *
     * This method accepts the prompt to be asked of the user. It then stores
     * the integer value and returns it to the method call.
     */
    public static int promptInt(String question) {
        int temp;
        System.out.print(question);
        temp = input.nextInt();
        input.nextLine();
        return temp;
    }

    /**
     * @author Jamison Peconi
     * @date 02/01/2016
     * @param array
     *
     * This method displays a String array. Accepts the array to be displayed as
     * a parameter.
     */
    public static void showStringArray(String[] array) {

        for (String val : array) {
            System.out.println(val);
        }

    }

    /**
     * @author Jamison Peconi
     * @date 02/01/2016
     * @param array
     *
     * This method displays an integer array. It accepts the array to be
     * displayed.
     */
    public static void showIntArray(int[] array) {
        for (int val : array) {
            System.out.println(val);
        }
    }

    /**
     * @author Jamison Peconi
     * @date 02/01/2016
     * @param array
     * @return
     *
     * This method accepts an array, it then reverses the values and stores them
     * in a new array. This is for Integers.
     */
    public static int[] reverseIntArray(int[] array) {
        int[] reverseArray = new int[array.length];
        for (int x = 0; x < array.length; x++) {
            reverseArray[array.length - (x + 1)] = array[x];
        }
        return reverseArray;
    }

}
